import React, {Component} from 'react';
import './DoorLock.css';
import {connect} from "react-redux";

class DoorLock extends Component {
  render() {
    return (
        <div className="lock-door">
          <div className="lock__display">*4*5</div>
          <div className="lock__inputs">
            <button className="lock__btn" onClick={this.props.addNum}>7</button>
            <button className="lock__btn" onClick={this.props.addNum}>8</button>
            <button className="lock__btn" onClick={this.props.addNum}>9</button>
            <button className="lock__btn">4</button>
            <button className="lock__btn">5</button>
            <button className="lock__btn">6</button>
            <button className="lock__btn">1</button>
            <button className="lock__btn">2</button>
            <button className="lock__btn">3</button>
            <button className="lock__btn">&lt;</button>
            <button className="lock__btn">0</button>
            <button className="lock__btn">E</button>
          </div>
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {pin : state.pin};
};

const mapDispatchToProps = dispatch => {
  return {
    addNum: (event) => dispatch({type: "ADD_NUM", value: event.target.innerHTML}),
    // subtractCounter: () => dispatch({type: actionsTypes.SUBTRACT, amount: 5}),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(DoorLock);
