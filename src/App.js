import React, { Component } from 'react';
import './App.css';
import DoorLock from "./containers/DoorLock/DoorLock";

class App extends Component {
  render() {
    return (
      <div className="App">
        <DoorLock/>
      </div>
    );
  }
}

export default App;
